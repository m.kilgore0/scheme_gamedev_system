#| the interface between graphics and game mechanics |#

;;maps people to their names for graphics purpose
;;maybe individual graphics for all entities
;;todo: maybe sussman character
(define (people-names-for-graphics)
  `((,my-avatar adventurer)
    (grendel troll)
    (registrar troll)
    (dr-evil housemaster)
    (mr-bigglesworth housemaster)
    (ben-bitdiddle male-student)
    (alyssa-hacker female-student)
    (course-6-frosh female-student)
    (lambda-man lambda-man)
    ))

(define (thing-names-for-graphics)
  `((sword sword)
    (health-potion health-potion)
    (magic-potion magic-potion)
    (treasure-chest treasure-chest)
    ))
	    
(define (update-graphics)
  ;; go per tile, placing everything at that tile
  ;; in the room
  (let ((clean-slate (make-empty-room))
	(tiles (get-tiles the-room))
	(my-column (get-tile-column (get-location my-avatar)))
	(my-row (get-tile-row (get-location my-avatar))))
    (begin
      (set-graphics! the-room clean-slate)
      ;;todo - redo place exit code
      (place-exits! the-room)
    (for-each
     (lambda(t)
       (populate-tile t))
     tiles)
       (place-person-in-room my-row
			    my-column
			    my-avatar)
    (print-health-bar! (get-health my-avatar) (get-foobars my-avatar))
    (print-room! (get-graphics the-room))
    (print-inventory!))))

(define (place-exits! room)
  (let ((east-exit (third (get-exits room)))
	(west-exit (fourth (get-exits room))))
    (begin
       (cond (east-exit
       (set-graphics!
	the-room
	(set-cell
      (get-graphics the-room)
      (get-offset east-exit)
      5
      'exit))))
       (cond (west-exit
       (set-graphics!
	the-room
	(set-cell
      (get-graphics the-room)
      (get-offset west-exit)
      0
      'entrance)))))))
(define (populate-tile tile)
  (let ((things (things-in-tile tile))
	(people (people-in-tile tile))
	(r (get-tile-row tile))
	(c (get-tile-column tile)))
    (begin
      (for-each
       (lambda(t)
	 (place-thing-in-room r c t))
       things)
      (for-each
       (lambda(p)
	 (place-person-in-room r c (get-name p)))
       people))))

(define (place-base-room r c)
  (let ((object 'empty-cell))
    (cond (object
	   (set-graphics!
	    the-room
	    (set-cell (get-graphics the-room)
		      r
		      c
		      object))))))

(define (place-thing-in-room r c thing)
  (let ((object (get-thing-name (get-name thing))))
    (cond (object
	   (set-graphics!
	    the-room
	    (set-cell (get-graphics the-room)
		      r
		      c
		      object))))))
(define (place-person-in-room r c person)
  (let ((object (get-person-name person)))
    (cond (object
	   (set-graphics!
	    the-room
	    (set-cell (get-graphics the-room)
		      r
		      c
		      object))))))

(define (get-thing-name thing)
  (let ((hit (assoc thing (thing-names-for-graphics))))
    (cond (hit (cadr hit))
	   (else 'unknown-thing))))
(define (get-person-name person)
  (let ((hit (assoc person (people-names-for-graphics))))
    (cond (hit (cadr hit))
	   (else 'unknown-person))))
(define (get-tile-row tile)
  (get-y tile)
  )
(define (get-tile-column tile)
  (get-x tile)
  )
