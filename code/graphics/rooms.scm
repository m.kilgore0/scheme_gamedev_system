#| Graphics for rooms |#

;;todo: clean this up (entrance/exit fields no longer needed)
(define (make-empty-room)
  (let ((entrance (random-entrance))
	(exit (random-exit)))
   ;; a room has 4 rows and 6 columns
   (list (make-list 4 (make-list 6 `(empty-cell))))))

;; specification is a list of pairs of (<item> <chance of appearance>)
;; chance of appearance is out of 100
;; chance of appearing twice is 1/2 the chance of appearing once
(define (make-random-room specification)
  (let ((room (make-empty-room)))
  (for-each
   (lambda(spec)
     (let ((r (random 100)))
       (cond ((> (cdr spec) r)
	      (set! room (place-randomly room (car spec)))))
       (cond ((> (floor (/ (cdr spec) 2)) r)
	      (set! room (place-randomly room (car spec)))))))
   specification)
  room))

(define (remove-from-cell object r c room)
  (let ((cell (get-cell r c room)))
    (overwrite-cell room r c (remove-from-list object (get-cell r c room)))
    ))

(define (get-cell r c room)
  (ith (ith (car room) r) c))

(define (remove-from-list item l)
  (cond ((null? l) `())
        ((eq? item (car l)) (cdr l))
	(else (append (list (car l))
		      (remove-from-list item (cdr l))))))
;; helper fxn for repeatedly doing a given action
(define (do-n-times operation n)
    (if (= n 0)
        "done"
        (begin
            (operation)
            (do-n-times operation (- n 1))
        )
    )
)
(define (ith list i)
  (car (sublist list i (+ 1 i))))

(define (place-randomly room obj)
  (set-cell room (random 3) (random 5) obj))
(define (set-cell room r c new-value)
  ;; adds new-value to the cell
  ;; does not modify in place
  (let ((cells (car room))
	(row (ith (car room) r))
	(old-list (get-cell r c room)))
    (cons
    (append
     (list-head cells r)
     (list (append
      (list-head row c)
      (list (append (list new-value) old-list))
      (list-tail row (+ c 1))))
     (list-tail cells (+ r 1)))
    (cdr room))))
(define (overwrite-cell room r c new-value)
  ;; does not modify in place
  (let ((cells (car room))
	(row (ith (car room) r)))
    (cons
    (append
     (list-head cells r)
     (list (append
      (list-head row c)
      (list new-value)
      (list-tail row (+ c 1))))
     (list-tail cells (+ r 1)))
    (cdr room))))

(define (move-object room object src dest)
  (set-cell
   (remove-from-cell object (car src) (cdr src) room)
   (car dest) (cdr dest) object))


(define (random-square)
  ;; row then column
  (cons (random 3) (random 5))
  )

(define (random-entrance)
  (cons (random 3) 0))
(define (random-exit)
  (cons (random 3) 5))
(define (random-edge-square)
  (let ((try (random-square)))
    (cond ((is-edge? try) try)
	  (else (random-edge-square)))))

(define (is-edge? square)
  ;; is edge if in column 0 or 5
  (or (eq? (cdr square) 0)
	     (eq? (cdr square) 5)))
(define (is-corner? square)
  (and (or (eq? (car square) 0)
	   (eq? (car square) 3))
       (or (eq? (cdr square) 0)
	   (eq? (cdr square) 5))))

(define (print-room! room)
  (display (get-header-text))
  (display (string*
	    (map (lambda (h r) (get-row-text h r))
		 (row-headers)
		 (car room))))
  (display (horizontal-divider))
  )

(define (get-header-text)
  (string-append
   (left-padding)
   "     A     "
   "     B     "
   "     C     "
   "     D     "
   "     E     "
   "     F     "
   "\n"))

(define (row-headers)
  (list
   (list "          "
	 "          "
	 "          "
	 "     1    "
	 "          "
	 "          "
	 "          ")
   (list "          "
	 "          "
	 "          "
	 "     2    "
	 "          "
	 "          "
	 "          ")
   (list "          "
	 "          "
	 "          "
	 "     3    "
	 "          "
	 "          "
	 "          ")
   (list "          "
	 "          "
	 "          "
	 "     4    "
	 "          "
	 "          "
	 "          ")))

(define (get-row-text row-header row)
  (string*
   (cons (horizontal-divider)
	 (map (lambda (c0 c1 c2 c3 c4 c5 c6)
		(string-append c0 c1 c2 c3 c4 c5 c6 "|\n"))
       row-header
       (cadr (assq (first (first row)) ascii-obj-alist))
       (cadr (assq (first (second row)) ascii-obj-alist))
       (cadr (assq (first (third row)) ascii-obj-alist))
       (cadr (assq (first (fourth row)) ascii-obj-alist))
       (cadr (assq (first (fifth row)) ascii-obj-alist))
       (cadr (assq (first (sixth row)) ascii-obj-alist))
	 ))))

(define (left-padding)
  "          ")

(define (horizontal-divider)
  (string-append (left-padding)
		 "+"
		 (string* (make-list 6 (square-top)))
		 "\n"))
(define (square-top)
  "----------+")


#| ----- ASCII representation for all objects |#



(define (entrance-graphics)
  (list
   "|          "
   "|          "
   "|<<        "
   "|          "
   "|          "
   ;;"|          "
   ))
(define (exit-graphics)
  (list
   "|          "
   "|          "
   "|        >>"
   "|          "
   "|          "
   ;;"|          "
   ))
(define (health-potion-graphics)
  (list
   "|    __    "
   "|    ||    "
   "|   \/  \\   "
   "|  | HP |  "
   "|  \\____\/  "
   ;;"|          "
   ))
(define (magic-potion-graphics)
  (list
   "|    __    "
   "|    ||    "
   "|   \/  \\   "
   "|  | MP |  "
   "|  \\____\/  "
   ;;"|          "
   ))
(define (treasure-chest-graphics)
  (list
   "|   ____   "
   ;;"|  \/    \\  "
   "|  ------  "
   "| |  ()  | "
   "| |  ||  | "
   "|  ------  "))

(define (sword-graphics)
  (list
   "|          "
   "|          "
   "| -|-----\\ "
   "| -|-----\/ "
   "|          "
   ;;"|          "
   ))

(define (troll-graphics)
  (list
   "|  \/\\  \/\\  "
   "| \/\/\\\\\/\/\\\\ "
   "| \\.\\  \/.\/ "
   "| | ---- | "
   "| \\______\/ "
   ;;"| v_\/  \\_v "
   ))

(define (adventurer-graphics)
  (list
   "|          "
   "| \\  (\")   "
   "|  \\--|--o "
   "|     :    "
   "|   _\/ \\_  "
   ;;"|          "
   ))
(define (male-student-graphics)
  (list
   "|          "
   "|    (\")   "
   "|  o--|--o "
   "|     :    "
   "|   _\/ \\_  "
   ;;"|          "
   ))   
(define (female-student-graphics)
  (list
   "|          "
   "|    (\")~  "
   "|  o--|--o "
   "|     :    "
   "|   _\/ \\_  "
   ;;"|          "
   ))
(define (lambda-man-graphics)
  (list
   "| __       "
   "|  o\\o     "
   "|   u\\     "
   "|    \/\\    "
   "|   \/  \\_  "
   ;;"|  \/    \\_ "
   ))
(define (housemaster-graphics)
  (list
   "|   o   o  "
   "|   |\\0\/|  "
   "|    \\Y\/   "
   "|    \/_\\   "
   "|   \/___\\  "
   ;;"|          "
   ))
(define (unknown-person)
  (list
   "|  person  "
   "|    ??    "
   "|   ????   "
   "|    ??    "
   "|  person  "
   ;;"|          "
   ))
(define (unknown-thing)
  (list
   "|  thing   "
   "|    ??    "
   "|   ????   "
   "|    ??    "
   "|   thing  "
   ;;"|          "
   ))
(define (wall-graphics)
  (list
   "|##########"
   "|##########"
   "|##########"
   "|##########"
   "|##########"
   ;;"|          "
   ))

(define (empty-cell)
  (make-list 5
	     "|          "))
(define ascii-obj-alist
  `((empty-cell ,(empty-cell))
    (health-potion ,(health-potion-graphics))
    (magic-potion ,(magic-potion-graphics))
  (treasure-chest ,(treasure-chest-graphics))
  (sword ,(sword-graphics))
  (entrance ,(entrance-graphics))
  (troll ,(troll-graphics))
  (exit ,(exit-graphics))
  (adventurer ,(adventurer-graphics))
  (female-student ,(female-student-graphics))
  (male-student ,(male-student-graphics))
  (lambda-man ,(lambda-man-graphics))
  (housemaster ,(housemaster-graphics))
  (unknown-person ,(unknown-person))
  (unknown-thing ,(unknown-thing))
  (wall-graphics ,(wall-graphics))))   

;;Helper function for debugging

(define (print-obj object)
  (display
   (string* (map
	     (lambda(s) (string-append s "\n"))
	     object))))
   
