
(define (print-health-bar! health-level foobars)
  (newline)
  (display (string-append
   (left-padding)
   (print-foobs foobars)
   (empty)
   (left-padding)
   "    Health: ["
   (health-gauge health-level)
   "\n")))

(define (empty)
  "           ")
(define (health-gauge level)
  (case level
    ((0) "          ]")
    ((1) "         =]")
    ((2) "        ==]")
    ((3) "       ===]")
    ((4) "      ====]")
    ((5) "     =====]")
    ((6) "    ======]")
    ((7) "   =======]")
    ((8) "  ========]")
    ((9) " =========]")
    ((10) "==========]")))
(define (print-foobs foobars)
  (string-pad-right
   (string-append
   "Foobars: "
   (number->string foobars)) 22)) 

;; note: no thing can have a name longer than 22 chars
(define (print-inventory!)
  (let ((things (get-things
	  (get-bag my-avatar))
	 ))
  (display
   (string-append
    (left-padding)
    "Inventory:\n"
    (left-padding)))
  (define print-items
    (lambda(items n)
      (if (not (null? items))
	  (begin
	    (cond ((equal? (remainder n 6) 0)
		   (begin
	     (newline)
	     (display (left-padding)))))
      (display (string-pad-right
	        (symbol->string (get-name (car items))) 22))
      (print-items (cdr items) (+ n 1))))))
  (if (null? things)
      (display "Empty")
  (print-items things 0))))
	    
    
