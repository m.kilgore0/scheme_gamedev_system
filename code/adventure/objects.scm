(load "adventure/base-objects")

(load "adventure/locations")

(load "adventure/mobile-things")

(load "adventure/people")

(load "adventure/bags")

(load "adventure/agents")

(load "adventure/ai")

(load "adventure/monsters")

(load "adventure/avatar")

(load "adventure/handlers")

(load "adventure/movement")
