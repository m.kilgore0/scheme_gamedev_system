;;; Avatars

(define avatar:screen
  (make-property 'screen
                 'predicate screen?))

(define avatar?
  (make-type 'avatar (list avatar:screen)))
(set-predicate<=! avatar? person?)

(define make-avatar
  (type-instantiator avatar?))

(define get-screen
  (property-getter avatar:screen avatar?))

(define (look-around avatar)
  (tell! (list "You are in room"
	       (get-tile-room (get-location avatar))
	       "at position"
	       `(,(get-x (get-location avatar))
		 ,(get-y (get-location avatar)))
	       "aka"
	       (get-location avatar))
         avatar)
  (let ((my-things (get-things avatar)))
    (if (n:pair? my-things)
        (tell! (cons "Your bag contains:" my-things)
               avatar)))
  (let ((things
         (append (things-here-tile avatar)
                 (people-here-tile avatar))))
    (if (n:pair? things)
        (tell! (append
		(list "You see here at position"
		      `(,(get-x (get-location avatar))
			,(get-y (get-location avatar)))
		      ":")
		things)
               avatar)))
  (let ((things
         (append (things-here-room avatar)
                 (people-here-room avatar))))
    (if (n:pair? things)
        (tell! (append
		(list "You see here in room"
		      (get-tile-room (get-location avatar))
		      ":")
		things)
               avatar)))
  (tell! (let ((exits (exits-here avatar)))
           (if (n:pair? exits)
               (cons "You can exit:"
                     exits)
               '("There are no exits..."
                 "you are dead and gone to heaven!")))
         avatar))
