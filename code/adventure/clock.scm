;;; Clock

(define (make-clock)
  (%make-clock 0 '()))

(define-record-type <clock>
    (%make-clock current-time things)
    clock?
  (current-time current-time set-current-time!)
  (things clock-things set-clock-things!))

(define (register-with-clock! thing clock)
  (set-clock-things! clock
                     (lset-adjoin eqv?
                                  (clock-things clock)
                                  thing)))

(define (unregister-with-clock! thing clock)
  (set-clock-things! clock
                     (delv thing (clock-things clock))))

(define (tick! clock)
  (set-current-time! clock (n:+ (current-time clock) 1))
  (for-each (lambda (thing) (clock-tick! thing))
            (clock-things clock)))

(define clock-tick!
  (chaining-generic-procedure 'clock-tick! 1
    (constant-generic-procedure-handler #f)))

(define (define-clock-handler type-predicate action)
  (define-generic-procedure-handler clock-tick!
    (match-args type-predicate)
    (lambda (super object)
      (super object)
      (action object))))

;;; Clock Hooks Object

(define-record-type <clock-hooks>
    (%make-hooks chain-mode hook-alist)
    clock-hooks?
  (chain-mode get-chain-mode set-chain-mode!)
  (hook-alist get-hook-alist set-hook-alist!))

(define global-hooks (%make-hooks 'global '()))

(define (the-global-hooks) global-hooks)

(define (%get-hook clock-hook-obj type-predicate)
  (let ((pair (assv type-predicate
		    (get-hook-alist clock-hook-obj))))
    (if pair
	(cdr pair)
	#f)))

(define (lookup-hook clock-hook-obj type-predicate)
  (let ((pair (assv type-predicate
		    (get-hook-alist clock-hook-obj)))
	(mode (get-chain-mode clock-hook-obj)))
    (if pair
	(cdr pair)
	(case mode
	  ((global) (error "Unset global hook predicate:"
			   type-predicate))
	  ((overwrite) (lookup-hook (the-global-hooks)
				    type-predicate))
	  (else (error "Invalid hook mode:" mode))))))
	    
(define (add-hook! clock-hook-obj type-predicate action)
  (let* ((hook-alist (get-hook-alist clock-hook-obj))
	 (pair (assv type-predicate hook-alist)))
    (if pair
	(set-cdr! pair action)
	(if (null? hook-alist)
	    (set-hook-alist! clock-hook-obj
			     (list (cons type-predicate
					 action)))
	    (set-cdr! (last-pair hook-alist)
		      (list (cons type-predicate
				  action)))))))

;;; Clock hook wrappers

(define (define-clock-hook type-predicate action)
  (add-hook! (the-global-hooks) type-predicate action)
  (define-clock-handler
    type-predicate
    (lambda (object)
      (let* ((local-hooks (get-hooks object))
	     (haction (lookup-hook (if local-hooks
				       local-hooks
				       (the-global-hooks))
				   type-predicate)))
	(haction object)))))
