;;; Hookable

(define hookable:hooks
  (make-property 'hooks
                 'predicate (lambda (x)
			      (or (not x)
				  (clock-hooks? x)))
		 'default-value #f))

(define hookable?
  (make-type 'hookable (list hookable:hooks)))
(set-predicate<=! hookable? object?)

(define make-hookable
  (type-instantiator hookable?))

(define get-hooks
  (property-getter hookable:hooks hookable?))

(define set-hooks!
  (property-setter hookable:hooks hookable? (lambda (x)
					      (or (not x)
						  (clock-hooks? x)))))

;;; Things

(define thing:location
  (make-property 'location
                 'predicate (lambda (x) (container? x))))

(define thing?
  (make-type 'thing (list thing:location)))
(set-predicate<=! thing? hookable?)

(define make-thing
  (type-instantiator thing?))

(define get-location
  (property-getter thing:location thing?))

;;; Containers

(define container:things
  (make-property 'things
                 'predicate (is-list-of thing?)
                 'default-value '()))

(define container?
  (make-type 'container (list container:things)))
(set-predicate<=! container? hookable?)

(define get-things
  (property-getter container:things container?))

(define add-thing!
  (property-adder container:things container? thing?))

(define remove-thing!
  (property-remover container:things container? thing?))

