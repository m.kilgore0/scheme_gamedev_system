;;; People

(define person:health
  (make-property 'health
                 'predicate n:exact-integer?
                 'default-value 10))

(define person:bag
  (make-property 'bag
                 'predicate (lambda (x) (bag? x))
                 'default-supplier
                 (lambda () (make-bag 'name 'my-bag))))
(define person:foobars
  (make-property 'foobars
                 'predicate (lambda (x) (integer? x))
                 'default-value 0))
(define person:is-cursed
  (make-property 'is-cursed
		 'predicate boolean?
		 'default-value #f))
;;for movement within rooms
(define loc?
  (lambda (x)
    (pair? x)))

(define person:loc
  (make-property `loc
		 `predicate loc?
		 `default-value (cons 1 1)))

(define person?
  (make-type 'person (list person:health person:bag person:loc
			   person:foobars person:is-cursed)))
(set-predicate<=! person? mobile-thing?)

(define get-foobars
  (property-getter person:foobars person?))
(define set-foobars!
  (property-setter person:foobars person? integer?))
(define get-cursed
  (property-getter person:is-cursed person?))
(define set-cursed!
  (property-setter person:is-cursed person? boolean?))

(define get-loc
  (property-getter person:loc person?))

(define set-loc!
  (property-setter person:loc person? loc?))

(define get-health
  (property-getter person:health person?))

(define set-health!
  (property-setter person:health person? n:exact-integer?))

(define get-bag
  (property-getter person:bag person?))
;;(define set-bag!
;;  (property-setter person:bag person? bag?))

(define (when-alive callback)
  (lambda (person)
    (if (n:> (get-health person) 0)
        (callback person))))

(define (people-here-tile person)
  (delv person (people-in-tile (get-location person))))

(define (things-here-tile person)
  (things-in-tile (get-location person)))

(define (people-here-room person)
  (delv person (people-in-room (get-tile-room (get-location person)))))

(define (things-here-room person)
  (things-in-room (get-tile-room (get-location person))))

(define (exits-here person)
  (get-exit-directions (get-location person)))

(define (peoples-things person)
  (append-map get-things (people-here-tile person)))

(define (suffer! hits person)
  (guarantee n:exact-integer? hits)
  (say! person (list "Ouch!" hits "hits is more than I want!"))
  (set-health! person (- (get-health person) hits))
  (if (< (get-health person) 1)
      (die! person)))
(define (curse! person)
  (set-cursed! person #t))
(define (undo-curse! person)
  (set-cursed! person #f))
;;todo jfs debug
(define (die! person)
  (for-each (lambda (thing)
              (drop-thing! thing person))
            (get-things person))
  (narrate! (list (get-name person) "has been killed!") person)
  (announce!
   (list "An earth-shattering, soul-piercing scream is heard..."))
  (set-health! person 0)
  (move! person (get-heaven) person))

(define (resurrect! person health)
  (guarantee n:exact-positive-integer? health)
  (set-health! person health)
  (move! person (get-origin person) person))
