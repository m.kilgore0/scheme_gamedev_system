;;; Thing Handlers

(define-generic-procedure-handler set-up! (match-args thing?)
  (lambda (super thing)
    (super thing)
    (add-thing! (get-location thing) thing)))

(define-generic-procedure-handler tear-down! (match-args thing?)
  (lambda (super thing)
    (remove-thing! (get-location thing) thing)
    (super thing)))

(define-generic-procedure-handler send-message!
  (match-args message? thing?)
  (lambda (message thing)
    unspecific))

;;; Location Handlers

(define-generic-procedure-handler set-up! (match-args exit?)
  (lambda (super exit)
    (super exit)
    ;; TODO - FIX
    ;;(add-exit! (get-from exit) exit)
    ))

;;from demo code:
;;(add-exit! (get-origin-room exit) exit)))

(define-generic-procedure-handler send-message!
  (match-args message? tile?)
  (lambda (message tile)
    (for-each (lambda (person)
                (send-message! message person))
              (people-in-room (get-tile-room tile)))))

;;; Mobile Thing Handlers

(define enter-tile!
  (chaining-generic-procedure 'enter-tile! 1
    (constant-generic-procedure-handler #f)))

(define leave-tile!
  (std-generic-procedure 'leave-tile! 1
    (constant-generic-procedure-handler #f)))

(define enter-room!
  (chaining-generic-procedure 'enter-room! 1
    (constant-generic-procedure-handler #f)))

(define leave-room!
  (std-generic-procedure 'leave-room! 1
    (constant-generic-procedure-handler #f)))

;;; People Handlers

(define-generic-procedure-handler set-up! (match-args person?)
  (lambda (super person)
    (super person)
    (set-holder! (get-bag person) person)))

(define-generic-procedure-handler get-things (match-args person?)
  (lambda (person)
    (get-things (get-bag person))))

(define-generic-procedure-handler enter-tile!
  (match-args person?)
  (lambda (super person)
    (super person)
    (collision? person)
    (narrate! (list person
		    "enters position"
		    `(,(get-x (get-location person))
		      ,(get-y (get-location person))))
              person)))

(define-generic-procedure-handler enter-room!
  (match-args person?)
  (lambda (super person) ;;jfs removed super for now
    (super person)
    (cond ((avatar? person)
    (set! the-room (get-tile-room (get-location my-avatar)))))
    (narrate! (list person
		    "enters room"
		    (get-tile-room (get-location person)))
              person)
    (let ((people (people-here-tile person)))
      (if (n:pair? people)
          (say! person (cons "Hi" people))))))

(define-generic-procedure-handler leave-room!
  (match-args person?)
  (lambda (super person) ;;jfs removed super for now
    (super person)
    (narrate! (list person
		    "leaves room"
		    (get-tile-room (get-location person)))
              person)
    (let ((people (people-here-tile person)))
      (if (n:pair? people)
          (say! person (cons "Bye" people))))))

;;; Agent Handlers

(define-generic-procedure-handler set-up!
  (match-args autonomous-agent?)
  (lambda (super agent)
    (super agent)
    (register-with-clock! agent (get-clock))))

(define-generic-procedure-handler tear-down!
  (match-args autonomous-agent?)
  (lambda (super agent)
    (unregister-with-clock! agent (get-clock))
    (super agent)))

;;(define-clock-handler autonomous-agent? move-and-take-stuff!)
(define-clock-hook autonomous-agent? move-and-take-stuff!)

;;; Monster Handlers

;;(define-clock-handler house-master? irritate-students!)
(define-clock-hook house-master? irritate-students!)

;;(define-clock-handler troll? eat-people!)
(define-clock-hook troll? eat-people!)

(define (check-cursed avatar)
  (display "HERE IN CHECK CURSED")
  (if
   (get-cursed avatar)
   (suffer! 1 avatar)))

;;; Avatar Handlers

(define-clock-hook avatar? check-cursed)


(define-generic-procedure-handler send-message!
  (match-args message? avatar?)
  (lambda (message avatar)
    (send-message! message (get-screen avatar))))

(define-generic-procedure-handler enter-tile!
  (match-args avatar?)
  (lambda (super avatar)
    (super avatar)
    (tick! (get-clock))))

(define-generic-procedure-handler enter-room!
  (match-args avatar?)
  (lambda (super avatar)
    (super avatar)
    (look-around avatar)
    (set! the-room (get-tile-room
		    (get-location my-avatar)))
    ;;todo: eventually the room should be populated
    (set-graphics! the-room (make-empty-room))))
