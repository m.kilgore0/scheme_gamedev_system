;;; Directions

(define directions '(N S E W))

(define (get-directions) directions)

(define (direction? d)
  (and (memq d directions) #t))

(define (direction-num dir)
  (case dir
      ((N) 0)
      ((S) 1)
      ((E) 2)
      ((W) 3)
      (else (error "Invalid direction:" dir))))

;;; Exits (REDO)

(define exit:from-dir
  (make-property 'from-dir
                 'predicate (lambda (x) (direction? x))))

(define exit:to
  (make-property 'to
                 'predicate (lambda (x) (tile? x))))

(define exit:offset
  (make-property 'offset
                 'predicate exact-nonnegative-integer?
		 'default-value 0))

(define exit?
  (make-type 'exit (list exit:from-dir exit:to exit:offset)))
(set-predicate<=! exit? object?)

(define make-exit
  (type-instantiator exit?))

;;; Tiles

(define tile:x
  (make-property 'x
                 'predicate (lambda (x) (exact-nonnegative-integer? x))))

(define tile:y
  (make-property 'y
                 'predicate (lambda (x) (exact-nonnegative-integer? x))))

(define tile:room
  (make-property 'room
                 'predicate (lambda (x) (room? x))))

(define tile?
  (make-type 'tile (list tile:x tile:y tile:room)))
(set-predicate<=! tile? container?)

(define make-tile
  (type-instantiator tile?))

;;; Rooms

(define room:rows
  (make-property 'rows
                 'predicate (lambda (x) (and
					 (exact-positive-integer? x)
					 (n:<= 4)))
		 'default-value 1))

(define room:cols
  (make-property 'cols
                 'predicate (lambda (x) (and
					 (exact-positive-integer? x)
					 (n:<= 6)))
		 'default-value 1))

(define room:graphics
  (make-property 'graphics
		 'predicate (lambda (x) (cond ((list? x) (equal? 3
								 (length
								  x)))
					      (else #f)))
		 'default-value (make-empty-room)))

(define room:tiles
  (make-property 'tiles
                 'predicate (lambda (x)
			      (and (n:list? x)
				   (every tile? x)))
		 'default-value '()))

(define room:exits
  (make-property 'exits
                 'predicate (lambda (x)
                              (and (n:list? x)
				   (n:= (length x) 4)
				   (every (lambda (ex)
					    (or (not ex)
						(exit? ex)))
					  x)))
                 'default-value (make-list 4 #f)))

(define room?
  (make-type 'room (list room:rows room:cols room:tiles room:exits
			 room:graphics)))
(set-predicate<=! room? hookable?)

(define make-room
  (type-instantiator room?))

;;; Getters & Setters

(define get-from-dir
  (property-getter exit:from-dir exit?))

(define get-to
  (property-getter exit:to exit?))

(define get-offset
  (property-getter exit:offset exit?))

(define set-offset!
  (property-setter exit:offset exit? exact-nonnegative-integer?))

(define get-tile-room
  (property-getter tile:room tile?))

(define get-x
  (property-getter tile:x tile?))

(define get-y
  (property-getter tile:y tile?))

(define get-graphics
  (property-getter room:graphics room?))
(define set-graphics!
  (property-setter room:graphics room? list?))
		   

(define get-tiles
  (property-getter room:tiles room?))

(define set-tiles!
  (property-setter room:tiles room? (lambda (x)
				      (and (n:list? x)
					   (every tile? x)))))

(define get-rows
  (property-getter room:rows room?))

(define get-cols
  (property-getter room:cols room?))

(define get-exits
  (property-getter room:exits room?))

(define set-exits!
  (property-setter room:exits room? (lambda (x)
				      (and (n:list? x)
					   (n:= (length x) 4)
					   (every (lambda (ex)
						    (or (not ex)
							(exit? ex)))
						  x)))))

;;; Utils (REDO)

;; (define (find-exit-in-direction direction tile)
;;   (find (lambda (exit)
;;           (eqv? (get-direction exit) direction))
;;         (get-exits tile)))

(define (people-in-tile tile)
  (filter person? (get-things tile)))

(define (people-in-room room)
  (append-map people-in-tile (get-tiles room)))

(define (things-in-tile tile)
  (remove person? (get-things tile)))

(define (things-in-room room)
  (append-map things-in-tile (get-tiles room)))

(define (all-things-in-tile tile)
  (append (things-in-tile tile)
          (append-map get-things (people-in-tile tile))))

(define (all-things-in-room room)
  (append-map all-things-in-tile (get-tiles room)))

(define (takeable-things tile)
  (append (filter mobile-thing? (things-in-tile tile))
          (append-map get-things (people-in-tile tile))))

(define (add-exit! room exit)
  (let ((exits (get-exits room))
	(num (direction-num (get-from-dir exit))))
    (list-set! exits num exit)))

(define (del-exit! room dir)
  (let ((exits (get-exits room))
	(num (direction-num dir)))
    (list-set! exits num exit)))

;;(define (exit-in-direction room dir offset)
;;  #f)
(define (exit-in-direction room dir offset)
  (let ((exit (list-ref (get-exits room)
			(direction-num dir))))
    (if (and exit
	     (n:= offset (get-offset exit)))
	(if (eq? dir (get-from-dir exit))
	    (get-to exit)
	    (error "Invalid exit setup:" `(,dir ,(get-from-dir exit))))
	#f)))

#|(define (exit-in-direction room dir offset)
  (let ((exit (list-ref (get-exits room)
			(direction-num dir)))
	(new-room (random-choice all-places)))
    ;;todo: removed exit check
    (if exit
	     ;;(n:= offset (get-offset exit)))
	;;(if (eq? dir (get-from-dir exit))
	    ;; jfs note: this just picks random room to go to, may
	    ;; want to change later
	    (begin
	      (set! the-room new-room)
	      ;;jfs todo: a little janky
	      (get-tile-by-coord new-room (random-choice (list 0 5))
				 (random 4)))
	    ;;(error "Invalid exit setup:" `(,dir ,(get-from-dir exit))))
	    #f)))|#


(define (get-tile-by-coord room x y)
  (let* ((tiles (get-tiles room))
	 (rows (get-rows room))
	 (cols (get-cols room))
	 (len (length tiles))
	 (size (* rows cols))
	 (ind (+ (* y cols) x)))
    (if (and (n:<= 0 ind)
	     (n:< ind len)
	     (n:<= 0 x)
	     (n:< x cols)
	     (n:<= 0 y)
	     (n:< y rows)
	     (n:= size len))
	(list-ref tiles ind)
	(error "Invalid tile request:" `((,x ,y ,ind) ,len ,size)))))

(define (get-tile-by-index room ind)
  (let* ((tiles (get-tiles room))
	 (rows (get-rows room))
	 (cols (get-cols room))
	 (len (length tiles))
	 (size (* rows cols)))
    (if (and (n:<= 0 ind)
	     (n:< ind len)
	     (n:= size len))
	(list-ref tiles ind)
	(error "Invalid tile request:" `(,ind ,len ,size)))))

(define (tile-in-direction tile dir)
  (let* ((room (get-tile-room tile))
	 (rows (get-rows room))
	 (cols (get-cols room))
	 (x (get-x tile))
	 (y (get-y tile))
	 (ind (+ (* y cols) x)))
    (case dir
      ((N) (if (n:= y 0)
	       (exit-in-direction room dir x)
	       (get-tile-by-index room (- ind cols))))
      ((S) (if (n:= y (- rows 1))
	       (exit-in-direction room dir x)
	       (get-tile-by-index room (+ ind cols))))
      ((E) (if (n:= x (- cols 1))
	       (exit-in-direction room dir y)
	       (get-tile-by-index room (+ ind 1))))
      ((W) (if (n:= x 0)
	       (exit-in-direction room dir y)
	       (get-tile-by-index room (- ind 1))))
      (else (error "Invalid direction:" dir)))))

(define (get-exit-directions tile)
  (let ((dirs '()))
    (for-each (lambda (dir)
		(if (tile-in-direction tile dir)
		    (set! dirs (append dirs `(,dir)))))
	      directions)
    dirs))

(define (find-exit-in-direction dir tile)
  (if (tile-in-direction tile dir)
      dir
      #f))
