;;; Primitive

(define (create-thing name location)
  (make-thing 'name name
              'location location))

(define (create-mobile-thing name location)
  (make-mobile-thing 'name name
                     'location location))

(define (create-tile name x y room)
  (make-tile 'name name
	     'x x
	     'y y
	     'room room))

(define (create-room name rows cols)
  (make-room 'name name
	     'rows rows
	     'cols cols))

(define (create-exit from-dir to offset)
  (make-exit 'name 'exit
             'from-dir from-dir
             'to to
	     'offset offset))

(define (create-student name home restlessness acquisitiveness)
  (make-student 'name name
                'location home
                'restlessness restlessness
                'acquisitiveness acquisitiveness))

(define (create-house-master name home restlessness irritability)
  (make-house-master 'name name
                     'location home
                     'restlessness restlessness
                     'acquisitiveness 1/10
                     'irritability irritability))

(define (create-troll name home restlessness hunger)
  (make-troll 'name name
              'location home
              'restlessness restlessness
              'acquisitiveness 1/10
              'hunger hunger))

(define (create-avatar name home)
  (make-avatar 'name name
               'location home
               'screen (make-screen 'name 'the-screen)))

(define (can-go-both-ways from direction reverse-direction to)
  (create-exit from direction to)
  (create-exit to reverse-direction from))

;;; General

(define (create-tiles room rows cols)
  (let loop ((tiles '())
	     (ind 0))
    (let ((x (modulo ind cols))
	  (y (quotient ind cols)))
      (if (n:= ind (* rows cols))
	  tiles
	  (loop (append tiles
			(list (create-tile (symbol (get-name room)
						   '- x '- y)
					   x y room)))
		(+ ind 1))))))

(define (init-room name rows cols)
  (let* ((room (make-room 'name name
			  'rows rows
			  'cols cols))
	 (tiles (create-tiles room rows cols)))
    (set-tiles! room tiles)
    (populate-room room)    
    room))


(define (init-exit room dir to offset)
    (let ((exit (create-exit dir to offset)))
    (add-exit! room exit)))

(define (populate-room room)
  (for-each
   (lambda(spec)
     (let ((r (random 100)))
       (cond ((> (cdr spec) r)
	      (place-random-tile room (car spec))))
       (cond ((> (floor (/ (cdr spec) 2)) r)
	      (place-random-tile room (car spec))))))
   default-room-spec))
(define (place-random-tile room object)
  (let ((tile (get-tile-by-index room  (random (length (get-tiles room))))))
    (create-mobile-thing object tile)))

(define (init-student name room restlessness acquisitiveness)
  (create-student name
		  (random-choice (get-tiles room))
		  restlessness
		  acquisitiveness))

(define (init-house-master name room restlessness irritability)
  (create-house-master name
		       (random-choice (get-tiles room))
		       restlessness
		       irritability))

(define (init-troll name room restlessness hunger)
  (create-troll name
		(random-choice (get-tiles room))
		restlessness
		hunger))

(define (init-avatar name room)
  (let ((avatar-loc (random-choice (get-tiles room)))
	(room-graphics (get-graphics room)))
    (begin
   (set-graphics! room (set-cell room-graphics (get-y avatar-loc) (get-x avatar-loc)
				 `adventurer))
   (let ((my-avatar (create-avatar name avatar-loc)))
     (set-loc! my-avatar (cons (get-y avatar-loc) (get-x avatar-loc)))
     my-avatar))))

(define (init-people rooms names)
  (append (init-students rooms (list-ref names 0))
          ;;(init-profs rooms)
          ;;(init-president rooms)
          (init-house-masters rooms (list-ref names 1))
          (init-trolls rooms (list-ref names 2))))

(define (init-students rooms names)
  (map (lambda (name)
         (init-student name
                         (random-choice rooms)
                         (random-bias 2)
                         (random-bias 3)))
       names))


(define (init-house-masters rooms names)
  (map (lambda (name)
         (init-house-master name
                              (random-choice rooms)
                              (random-bias 3)
                              (random-bias 3)))
       names))

(define (init-trolls rooms names)
  (map (lambda (name)
         (init-troll name
                       (random-choice rooms)
                       (random-bias 3)
                       (random-bias 3)))
       names))

