(define (test-debug-env my-name)
  (%start-game my-name
	       debug-world
	       (lambda ()
		 (init-debug-people all-places)))
  (update-graphics))


(define (small-world)
  (let ((room-2 (init-room 'room-2 4 6))
	(room-3 (init-room 'room-3 4 6))
	(room-4 (init-room 'room-4 4 6))
	(room-5 (init-room 'room-5 4 6))
	(room-6 (init-room 'room-6 4 6)))
    (init-exit room-3 'W (get-tile-by-coord room-2 5 1) 1)
    (init-exit room-2 'W (get-tile-by-coord room-4 5 3) 3)
    ;;(init-exit room-4 'W (get-tile-by-coord room-5 5 0) 0)
    (init-exit room-3 'E (get-tile-by-coord room-4 0 0) 0)
    ;;(init-exit room-4 'E (get-tile-by-coord room-2 0 3) 3)
    (init-exit room-2 'E (get-tile-by-coord room-6 0 2) 2)
    (list room-2 room-3 room-4 room-5 room-6)))
(define (start-adventure my-name)
  (%start-game my-name
	       small-world
	       (lambda ()
		 (init-people all-places entities)))
  (update-graphics))

(define (init-small-world-people rooms)
  (let ((p1 (init-student 'lambda-man
                          (random-choice rooms)
                          (random-bias 2)
                          (random-bias 3)))
	(p2 (init-student 'course-6-frosh
                          (random-choice rooms)
                          (random-bias 2)
                          (random-bias 3)))
	 (p3 (init-student 'ben-bitdiddle
                          (random-choice rooms)
                          (random-bias 2)
                          (random-bias 3)))
	  (p4 (init-student 'alyssa-hacker
                          (random-choice rooms)
                          (random-bias 2)
                          (random-bias 3)))
	   (p5 (init-troll 'grendel
                          (random-choice rooms)
                          (random-bias 2)
                          (random-bias 3)))
	    (p6 (init-troll 'registrar
                          (random-choice rooms)
                          (random-bias 2)
                          (random-bias 3)))
	     (p8 (init-house-master 'dr-evil
                          (random-choice rooms)
                          (random-bias 2)
                          (random-bias 3)))
	     (p9 (init-house-master 'mr-bigglesworth
                          (random-choice rooms)
                          (random-bias 2)
                          (random-bias 3)))
	     (hooks (%make-hooks 'overwrite '())))
    (define (override-agent! agent)
      (say! agent '("I'm not calling move-and-take-stuff!")))
    (add-hook! hooks
	       autonomous-agent?
	       override-agent!)
    (list p1 p2 p4 p5 p6 p8 p9)))
(define (init-debug-people rooms)
  (let ((p1 (init-student 'lambda-man
                          (random-choice rooms)
                          (random-bias 2)
                          (random-bias 3)))
	;;(p2 (init-student 'test-man
        ;;                  (random-choice rooms)
        ;;                  (random-bias 2)
        ;;                  (random-bias 3)))
	(hooks (%make-hooks 'overwrite '())))
    (define (override-agent! agent)
      (say! agent '("I'm not calling move-and-take-stuff!")))
    (add-hook! hooks
	       autonomous-agent?
	       override-agent!)
    ;;(set-hooks! p2 hooks)
    (list p1)))
