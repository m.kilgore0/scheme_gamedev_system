;;; AI

(define people-names '(ben-bitdiddle alyssa-hacker course-6-frosh lambda-man))

(define house-master-names '(dr-evil mr-bigglesworth))

(define troll-names '(grendel registrar))

(define entities (list people-names
		       house-master-names
		       troll-names))

;;;; The World

(define (create-world)
  (let ((start-room (init-room 'start-room 4 6))
        (loop-room (init-room 'loop-room 4 6)))
    (init-exit start-room 'W (get-tile-by-coord loop-room 5 1) 1)
    (init-exit loop-room 'W (get-tile-by-coord start-room 5 1) 1)
    (list start-room loop-room)))
