;;;; An adventure game at MIT

(define the-clock)
(define all-places)
(define heaven)
(define all-people)
(define my-avatar)
(define the-room)
(define default-room-spec
  (list
   (cons `health-potion 65)
   (cons `magic-potion 60)
   (cons `treasure-chest 50)
   (cons `sword 40)))

(define (start-adventure my-name)
  (%start-game my-name
	       create-world
	       (lambda ()
		 (init-people all-places entities)))
  (update-graphics))

(define (%start-game my-name init-world init-ai)
  (begin
    (set! the-clock (make-clock))
    (set! all-places (init-world))
    (set! heaven (init-room 'heaven 1 1))
    (set! all-people (init-ai))
    (set! the-room (random-choice all-places))
    ;;todo: add things to rooms somewhere
    (set-graphics! the-room
		   (make-empty-room))
    (set! my-avatar
          (init-avatar my-name the-room))))

(define (get-all-places)
  all-places)

(define (get-heaven)
  heaven)

(define (get-heaven-tile)
  (list-ref (get-tiles heaven) 0))

(define (get-clock)
  the-clock)

;;; User interface

(define (go direction)
  (let ((exit
         (find-exit-in-direction direction
                                 (get-location my-avatar))))
    (begin
      (if exit
        (take-exit! exit my-avatar)
        (narrate! (list "No exit in" direction "direction")
                  my-avatar))
      (update-graphics)))
  unspecific)


;;todo: hacky solution to get demo working
(define (take-exit-portal! direction object)
  (narrate! (list "You are entering a new room...")
		  my-avatar)
  ;;reset the room and place the avatar at a random edge position
  (let ((new-room (random-choice all-places)))
    (begin
      (set! the-room new-room)
      (let ((destination
	     (get-tile-by-coord new-room (random-choice (list 1 2 3 4))
				(random 4))))
	(move-internal! object (get-location object) destination)))))

;; deprecated
#|(define (exit-here? direction tile)
  (let ((x-loc (get-x tile))
	(y-loc (get-y tile)))
  (let ((exits-here
	  (filter
     (lambda(n)
       n)
     (map
      (lambda(e)
	(if e
       (if (and (equal? x-loc
		    (get-exit-x e))
	    (equal? y-loc
		    (get-exit-y e))
	    (equal? direction
		    (get-from-dir e)))
	   e
	   #f)
	#f))
      (get-exits the-room)))))
    (if (not (null? exits-here))
	(car exits-here)
	#f))))|#

;; for movement within a room
;; interface itself
;;todo: this needs to interface properly with existing movement code
(define (goto column row)
  (cond ((valid-coords row column)
  (let ((new-loc (cons (- row 1) (cadr
			  (assoc column
				 (column-mapping)))))
	(room (get-graphics the-room)))
    (begin
      (move-internal! my-avatar
		      (get-location my-avatar)
		      (get-tile-at (car new-loc) (cdr new-loc)))
      (update-graphics))))
	(else (display "Oops! Those coordinates aren't valid"))))

(define (get-tile-at r c)
  (let ((tiles (get-tiles the-room)))
    (car (filter (lambda(t)
	      (and
	     (equal? r (get-y t))
	     (equal? c (get-x t))))
	    tiles))))
(define (valid-coords r c)
  (and
   (> r 0)
   (< r 5)
   (assoc c (column-mapping))))

(define (take-thing name)
  (let ((thing (find-thing name (here))))
    (begin
    (if thing
        (take-thing! thing my-avatar))
    (update-graphics)))
  unspecific)

(define (drop-thing name)
  (let ((thing (find-thing name my-avatar)))
    (begin
    (if thing
        (drop-thing! thing my-avatar))
  (update-graphics)))
  unspecific)

(define (look-in-bag #!optional person-name)
  (let ((person
         (if (default-object? person-name)
             my-avatar
             (find-person person-name))))
    (if person
        (tell! (let ((referent (local-possessive person))
                     (things (get-things person)))
                 (if (n:pair? things)
                     (cons* referent "bag contains" things)
                     (list referent "bag is empty")))
               my-avatar)))
  unspecific)

(define (whats-here)
  (look-around my-avatar)
  unspecific)

(define (say . message)
  (say! my-avatar message)
  unspecific)

(define (tell person-name . message)
  (tell! message (find-person person-name))
  unspecific)

(define (hang-out ticks)
  (begin 
  (do ((i 0 (n:+ i 1)))
      ((not (n:< i ticks)))
    (tick! (get-clock)))
  (update-graphics))
  unspecific)


(define (use object-name)
  (let ((my-things (map
		    (lambda(n) (get-name n))
		    (get-things
		    (get-bag my-avatar)))))
    (if (member object-name my-things)
	(begin
	(cond
	 ((equal? object-name 'health-potion)
	 (begin
	   (narrate! (list (get-name my-avatar) "drank a health potion")
		     my-avatar)
	   (set-health! my-avatar 10)
	   (update-graphics)
	   )))
	(cond
	 ((equal? object-name 'magic-potion)
	 (begin
	   (narrate! (list (get-name my-avatar) "drank a magic
potion") my-avatar)
	   (cond ((get-cursed my-avatar)
		  (begin
		    (narrate! (list "The curse is lifted!") my-avatar)
		  (set-cursed! my-avatar #f))))
	   (update-graphics)
	   )))))))
;;todo: how to remove from bag
#|(define (remove-from-bag object)
  (let ((current-things (get-things
			 (get-bag my-avatar))))
    (set-bag! my-avatar (make-bag (delete-1st object
					      current-things)))))

  (define (delete-1st x lst)
  (cond ((null? lst) '())
        ((equal? (car lst) x) (cdr lst))
        (else (cons (car lst)
                    (delete-1st x (cdr lst))))))|#

;;; Support for UI

(define (here)
  (get-location my-avatar))

(define (find-person name)
  (let ((person
         (find-object-by-name name (people-here-room my-avatar))))
    (if (not person)
        (tell! (list "There is no one called" name "here")
               my-avatar))
    person))

(define (find-thing name person-or-place)
  (let ((thing
         (find-object-by-name
          name
          (person-or-place-things person-or-place))))
    (if (not thing)
        (tell! (cons* "There is nothing called"
                      name
                      (person-or-place-name person-or-place))
               my-avatar))
    thing))

(define (person-or-place-things person-or-place)
  (if (place? person-or-place)
      (all-things-in-place person-or-place)
      (get-things person-or-place)))

(define (person-or-place-name person-or-place)
  (if (place? person-or-place)
      '("here")
      (list "in" (local-possessive person-or-place) "bag")))

(define (local-possessive person)
  (if (eqv? person my-avatar)
      "Your"
      (possessive person)))

;; helpers
(define (column-mapping)
  `((A ,0)
    (B ,1)
    (C ,2)
    (D ,3)
    (E ,4)
    (F ,5)))

;;todo: make sure that mobile thing doesn't include people
;;todo: fix
(define (arrive-at-loc room person loc)
  (let ((things-here (filter
		      (lambda(n)
			(member n (mobile-things)))
		      (things-in-loc room loc))))
    (cond ((not (null? things-here))
	   (take-thing! (car things-here) person)))))
;;todo: this should just be a temporary measure until this is better
;;integrated
;;todo: also, treasure chest shouldn't really be mobile
(define (mobile-things)
  `(health-potion
    magic-potion
    treasure-chest
    sword))
(define (things-in-loc room loc)
  (list-ref
		      (list-ref
		      (car room)
		      (car loc)) (cdr loc)))
