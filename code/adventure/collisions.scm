;; Collision code using generic procedures
(define (collision? person)
  (let ((things-here (things-in-tile (get-location person)))
	(people-here (people-in-tile (get-location person))))
    (cond ((not (null? things-here)) (collision
				      person
				      (last_element things-here))))
        (cond ((not (null? people-here)) (collision
				      person
				      (last_element people-here))))))

(define (last_element l)
  (cond ((null? (cdr l)) (car l))
        (else (last_element (cdr l)))))

(define (open-chest)
  (let ((amount (random-choice (list 10 100 500 1000 6945))))
    (set-foobars!
     my-avatar
     (+ (get-foobars my-avatar) amount))
    (narrate! (list "The chest contained" (number->string
						    amount)
		    "foobars") my-avatar)))


(define collision
  (std-generic-procedure 'collision 2 #f))

;; todo : debug ordering
(define (treasure-chest? object)
  (equal? (get-name object)
	  'treasure-chest))
(define (inanimate-object? object)
  (and (mobile-thing? object)
       (not (person? object))))
(register-predicate! inanimate-object? 'inanimate-object)
(register-predicate! treasure-chest? 'treasure-chest)
(set-predicate<=! treasure-chest? inanimate-object?)
(set-predicate<=! inanimate-object? mobile-thing?)
;; default collision code
(define-generic-procedure-handler collision
  (match-args mobile-thing? mobile-thing?)
  (lambda (thing1 thing2)
    (values)))
(define-generic-procedure-handler collision
  (match-args avatar? troll?)
	      (lambda (avatar troll)
	        (begin
		    (narrate! (list (get-name troll) "attacks" (get-name
							    avatar))
			      avatar)
		    (suffer! (random 9) avatar))))
(define-generic-procedure-handler collision
  (match-args avatar? student?)
	      (lambda (avatar student)
	        (say! student (list "Excuse me," (get-name avatar)))
		))
(define-generic-procedure-handler collision
  (match-args avatar? inanimate-object?)
	      (lambda (avatar thing)
	        (begin
		  (cond ((equal? (get-name thing)
				 'treasure-chest)
			 (open-chest))
			(else
		(take-thing! thing avatar))))))
(define-generic-procedure-handler collision
  (match-args avatar? house-master?)
  (lambda (avatar housemaster)
    ;;todo - implement the consequences of being cursed
    (narrate! (list (get-name housemaster) "curses" (get-name
						  avatar))
	      avatar)    
    (curse! avatar)
    ))
